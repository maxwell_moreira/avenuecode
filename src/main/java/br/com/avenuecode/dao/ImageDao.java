package br.com.avenuecode.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.avenuecode.config.EntityManagerConfig;
import br.com.avenuecode.dao.interfaces.ImageDaoInterface;
import br.com.avenuecode.model.ImageModel;

/* Class responsible for representing the dao layer of the image object. */

@Repository("ImageDao")
public class ImageDao implements ImageDaoInterface {
	
	private EntityManagerConfig entityManagerConfig;
	
	/* Constructor */
	
	public ImageDao () {
		
		this.entityManagerConfig = EntityManagerConfig.getInstance();
		
	}
	
	/* Method responsible for persisting a image object. */
	
	public void insert (final ImageModel imageModel) {
		
		this.entityManagerConfig.getEntityManager().persist(imageModel);
		
	}

	/* Method responsible for update a image object. */
	
	public ImageModel update (final ImageModel imageModel) {
		
		ImageModel imageModelResult = this.entityManagerConfig.getEntityManager().merge(imageModel);
		this.entityManagerConfig.getEntityManager().flush();
		this.entityManagerConfig.getEntityManager().refresh(imageModelResult);
		
		return imageModelResult;
		
	}

	/* Method responsible for delete a image object. */
	
	public void delete (final ImageModel imageModel) {
		
		this.entityManagerConfig.getEntityManager().remove(imageModel);
		
	}

	/* Contract used to select by id of image. */
	
	public ImageModel selectById (final Long id) {
		
		ImageModel imageModel = this.entityManagerConfig.getEntityManager().find(ImageModel.class, id);
		
		return imageModel;
		
	}

	/* Contract used to select images based on a context. */
	
	public List<ImageModel> select (final String context) {
		
		List<ImageModel> imageModelList = this.entityManagerConfig.getEntityManager().createQuery(context, ImageModel.class).getResultList();
		
		return imageModelList;
		
	}
}
