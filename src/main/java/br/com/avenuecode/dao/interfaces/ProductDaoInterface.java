package br.com.avenuecode.dao.interfaces;

import java.util.List;

import br.com.avenuecode.model.ProductModel;

/* Class responsible for representing the contract used in the infrastructure layer for the persistence of the product object. */

public interface ProductDaoInterface {
	
	/* Method responsible for insert product. */
	
	public void insert (final ProductModel productModel);
	
	/* Method responsible for update product. */
	
	public ProductModel update (final ProductModel productModel);
	
	/* Method responsible for delete product. */
	
	public void delete (final ProductModel productModel);
	
	/* Method responsible for select by id of product. */
	
	public ProductModel selectById (final Long id);
	
	/*Method responsible for select products based on a context. */
	
	public List<ProductModel> select (final String context);

}
