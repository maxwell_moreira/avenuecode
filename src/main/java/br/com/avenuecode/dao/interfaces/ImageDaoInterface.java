package br.com.avenuecode.dao.interfaces;

import java.util.List;

import br.com.avenuecode.model.ImageModel;

/* Class responsible for representing the contract used in the infrastructure layer for the persistence of the image. */

public interface ImageDaoInterface {
	
	/* Method responsible for insert image. */
	
	public void insert (final ImageModel imageModel);
	
	/* Method responsible for update image. */
	
	public ImageModel update (final ImageModel imageModel);
	
	/* Method responsible for delete image. */
	
	public void delete (final ImageModel imageModel);
	
	/* Method responsible for select by id of image. */
	
	public ImageModel selectById (final Long id);
	
	/* Method responsible for select images based on a context. */
	
	public List<ImageModel> select (final String context);

}
