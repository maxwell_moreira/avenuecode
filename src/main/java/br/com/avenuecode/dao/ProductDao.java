package br.com.avenuecode.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.avenuecode.config.EntityManagerConfig;
import br.com.avenuecode.dao.interfaces.ProductDaoInterface;
import br.com.avenuecode.model.ProductModel;

/* Class responsible for representing the dao layer of the product object. */

@Repository("ImageDao")
public class ProductDao implements ProductDaoInterface {

	private EntityManagerConfig entityManagerConfig;

	/* Constructor */

	public ProductDao() {

		this.entityManagerConfig = EntityManagerConfig.getInstance();

	}

	/* Method responsible for persisting a product object. */

	public void insert (final ProductModel productModel) {
		
		this.entityManagerConfig.getEntityManager().persist(productModel);
		
	}
	
	/* Method responsible for update a product object. */

	public ProductModel update (final ProductModel productModel) {
		
		ProductModel productModelResult = this.entityManagerConfig.getEntityManager().merge(productModel);
		this.entityManagerConfig.getEntityManager().flush();
		this.entityManagerConfig.getEntityManager().refresh(productModelResult);
		
		return productModelResult;
		
	}
	
	/* Method responsible for remove a product object. */

	public void delete (final ProductModel productModel) {
		
		this.entityManagerConfig.getEntityManager().remove(productModel);
		
	}
	
	/* Contract used to select by id of product. */

	public ProductModel selectById (final Long id) {
		
		ProductModel productModel = this.entityManagerConfig.getEntityManager().find(ProductModel.class, id);
		
		return productModel;
		
	}
	
	/* Contract used to select products based on a context. */

	public List<ProductModel> select (final String context) {
		
		List<ProductModel> productModelList = this.entityManagerConfig.getEntityManager().createQuery(context, ProductModel.class).getResultList();
		
		return productModelList;
		
	}
	
}
