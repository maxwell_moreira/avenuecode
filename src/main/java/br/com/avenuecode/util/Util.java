package br.com.avenuecode.util;

/* Class responsible for providing utility methods. */

public class Util {
	
	public static final String GENERAL_VALIDATION_CODE_REQUIRED_FIELD = "general.validation.code.required.field";
	public static final String GENERAL_VALIDATION_MESSAGE_REQUIRED_FIELD = "general.validation.message.required.field";

}
