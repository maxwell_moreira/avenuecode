package br.com.avenuecode.business;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import br.com.avenuecode.business.interfaces.ImageBusinessInterface;
import br.com.avenuecode.config.PropertieConfig;
import br.com.avenuecode.dao.interfaces.ImageDaoInterface;
import br.com.avenuecode.model.ImageModel;
import br.com.avenuecode.util.Util;

/* Class responsible for representing the business layer of the image. */

@Service("ImageBusiness")
public class ImageBusiness implements ImageBusinessInterface {

	@Resource
	private ImageDaoInterface imageDaoInterface;

	/* Method responsible for persisting a product object. */

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void insert(final ImageModel imageModel) {

		Boolean isValid = imageModel.validateRequiredFields();

		if (isValid) {

			this.imageDaoInterface.insert(imageModel);

		} else {

			throw new RuntimeException(PropertieConfig.getInstance()
					.getMessage(Util.GENERAL_VALIDATION_MESSAGE_REQUIRED_FIELD));

		}
	}

	/* Method responsible for update a product object. */

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ImageModel update(final ImageModel imageModel) {

		Boolean isValid = imageModel.validateRequiredFields();

		if (isValid) {

			return this.imageDaoInterface.update(imageModel);

		} else {

			throw new RuntimeException(PropertieConfig.getInstance()
					.getMessage(Util.GENERAL_VALIDATION_MESSAGE_REQUIRED_FIELD));

		}
	}

	/* Method responsible for remove a product object. */

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void delete(final ImageModel imageModel) {

		if (imageModel != null && imageModel.getId() != null) {

			this.imageDaoInterface.delete(imageModel);

		}
	}

	/* Contract used to select by id of product. */

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public ImageModel selectById(final Long id) {

		ImageModel imageModel = null;

		if (id != null) {

			imageModel = this.imageDaoInterface.selectById(id);

		}

		return imageModel;

	}

	/* Contract used to select products based on a context. */

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<ImageModel> selectAll() {

		final String SELECT_ALL = "SELECT FROM ImageModel";

		return this.imageDaoInterface.select(SELECT_ALL);

	}
}
