package br.com.avenuecode.business;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.avenuecode.business.interfaces.ProductBusinessInterface;
import br.com.avenuecode.config.PropertieConfig;
import br.com.avenuecode.dao.interfaces.ProductDaoInterface;
import br.com.avenuecode.model.ProductModel;
import br.com.avenuecode.util.Util;

/* Class responsible for representing the business layer of the product. */

@Service("ProductBusiness")
public class ProductBusiness implements ProductBusinessInterface {

	@Resource
	private ProductDaoInterface productDaoInterface;

	/* Method responsible for persisting a product object. */

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void insert(final ProductModel productModel) {

		Boolean isValid = productModel.validateRequiredFields();

		if (isValid) {

			this.productDaoInterface.insert(productModel);

		} else {

			throw new RuntimeException(PropertieConfig.getInstance()
					.getMessage(Util.GENERAL_VALIDATION_MESSAGE_REQUIRED_FIELD));

		}
	}

	/* Method responsible for update a product object. */

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ProductModel update(final ProductModel productModel) {

		Boolean isValid = productModel.validateRequiredFields();

		if (isValid) {

			return this.productDaoInterface.update(productModel);

		} else {

			throw new RuntimeException(PropertieConfig.getInstance()
					.getMessage(Util.GENERAL_VALIDATION_MESSAGE_REQUIRED_FIELD));

		}
	}

	/* Method responsible for remove a product object. */

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void delete(final ProductModel productModel) {

		if (productModel != null && productModel.getId() != null) {

			this.productDaoInterface.delete(productModel);

		}
	}

	/* Contract used to select by id of product. */

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public ProductModel selectById(final Long id) {

		ProductModel productModel = null;

		if (id != null) {

			productModel = this.productDaoInterface.selectById(id);

		}

		return productModel;
	}

	/* Contract used to select products based on a context. */

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<ProductModel> selectAll() {

		final String SELECT_ALL = "SELECT FROM ProductModel";
		
		List<ProductModel> productModelList = this.productDaoInterface.select(SELECT_ALL);

		return productModelList;
	}
}
