package br.com.avenuecode.business.interfaces;

import java.util.List;

import br.com.avenuecode.model.ProductModel;

/* Class responsible for representing the contract used in the business layer of the product. */

public interface ProductBusinessInterface {

	/* Method responsible for insert product. */

	public void insert (final ProductModel productModel);

	/* Method responsible for update product. */

	public ProductModel update (final ProductModel productModel);

	/* Method responsible for delete product. */

	public void delete (final ProductModel productModel);

	/* Method responsible for select by id of product. */

	public ProductModel selectById (final Long id);

	/* Method responsible for select products based on a context. */

	public List<ProductModel> selectAll ();

}
