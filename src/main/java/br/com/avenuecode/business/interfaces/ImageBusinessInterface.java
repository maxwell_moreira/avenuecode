package br.com.avenuecode.business.interfaces;

import java.util.List;

import br.com.avenuecode.model.ImageModel;

/* Class responsible for representing the contract used in the business layer of the image. */

public interface ImageBusinessInterface {

	/* Method responsible for insert image. */

	public void insert(final ImageModel imageModel);

	/* Method responsible for update image. */

	public ImageModel update(final ImageModel imageModel);

	/* Method responsible for delete image. */

	public void delete(final ImageModel imageModel);

	/* Method responsible for select by id of image. */

	public ImageModel selectById(final Long id);

	/* Method responsible for select images based on a context. */

	public List<ImageModel> selectAll();

}
