package br.com.avenuecode.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/* Class responsible for representing the image. */

@Entity
@Table(name="IMAGE")
public class ImageModel implements Serializable {

	private static final long serialVersionUID = 9039576478514185148L;

	@Id
	@GeneratedValue
	@Column(name = "IMAGE_ID")
	private Long id;
	
	@Column(name = "IMAGE_NAME", nullable = false, length = 30)
	private String name;
	
	@Column(name = "IMAGE_URL", nullable = false, length = 100)
	private String url;
	
	/* Constructor */
	
	public ImageModel() {
		
	}
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUrl() {
		return this.url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	/* Method responsible for validating the required fields of the image object. */

	public Boolean validateRequiredFields() {

		Boolean result = true;

		// name

		if (this.name != null && !this.name.equals("")) {

			result = false;

		}

		// url

		if (this.url != null && !this.url.equals("")) {

			result = false;

		}

		return result;

	}
}
