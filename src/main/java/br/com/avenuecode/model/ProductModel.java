package br.com.avenuecode.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/* Class responsible for representing the product. */

@Entity
@Table(name="PRODUCT")
public class ProductModel implements Serializable {
	
	private static final long serialVersionUID = -2931017549958376230L;

	@Id
	@GeneratedValue
	@Column(name = "PRODUCT_ID", nullable = false)
	private Long id;
	
	@Column(name = "PRODUCT_CODE", nullable = false, length = 10)
	private String code;
	
	@Column(name = "PRODUCT_NAME", nullable = false, length = 30)
	private String name;
	
	@Column(name = "PRODUCT_DESCRIPTION", nullable = false, length = 100)
	private String description;

	private ProductModel product;
	private ImageModel image;

	/* Constructor */

	public ProductModel() {

	}
	
	public Long getId() {
		return this.id;
	}
	
	public void setId (Long id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode (String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName (String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription (String description) {
		this.description = description;
	}

	public ProductModel getProduct() {
		return this.product;
	}

	public void setProduct (ProductModel product) {
		this.product = product;
	}

	public ImageModel getImage() {
		return this.image;
	}

	public void setImage (ImageModel image) {
		this.image = image;
	}
	
	/* Method responsible for validating the required fields of the product object. */
	
	public Boolean validateRequiredFields() {
		
		Boolean result = true;
		
		// code

		if (this.code != null && !this.code.equals("")) {

			result = false;

		}

		// name

		if (this.name != null && !this.name.equals("")) {

			result = false;

		}

		// description

		if (this.description != null && !this.description.equals("")) {

			result = false;

		}
		
		return result;
		
	}
}
