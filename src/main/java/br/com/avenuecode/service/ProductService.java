package br.com.avenuecode.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.avenuecode.business.interfaces.ProductBusinessInterface;
import br.com.avenuecode.config.ApplicationContextConfig;
import br.com.avenuecode.model.ProductModel;

/* Class responsible for representing the service layer of the product object. */

@Path("/product")
public class ProductService {
	
	private static final String PRODUCT_BUSINESS = "ProductBusiness";
	private static final Integer TWO_HUNDRED = 200; 
	
	private ProductBusinessInterface productBusinessInterface;
	
	/* Constructor */
	
	public ProductService() {
		
		this.productBusinessInterface = (ProductBusinessInterface) ApplicationContextConfig.getInstance().getAbstractApplicationContext().getBean(PRODUCT_BUSINESS);
		
	}
	
	@POST
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response insert (final ProductModel productModel) {
		
		this.productBusinessInterface.insert(productModel);
		
		return Response.status(TWO_HUNDRED).entity(productModel).build();	
	}
	
	@PUT
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update (final ProductModel productModel) {
		
		ProductModel productModelResult = this.productBusinessInterface.update(productModel);
		
		return Response.status(TWO_HUNDRED).entity(productModelResult).build();	
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/delete")
	public void delete (final ProductModel productModel) {
		
		this.productBusinessInterface.delete(productModel);
	
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/select")
	public List<ProductModel> selectAll () {
		
		return this.productBusinessInterface.selectAll();
	
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/select/{id}")
	public ProductModel selectById (@PathParam("id") final Long idProduct) {
		
		return this.productBusinessInterface.selectById(idProduct);
	
	}
}
