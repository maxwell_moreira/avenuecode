package br.com.avenuecode.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.avenuecode.business.interfaces.ImageBusinessInterface;
import br.com.avenuecode.config.ApplicationContextConfig;
import br.com.avenuecode.model.ImageModel;

/* Class responsible for representing the service layer of the image object. */

@Path("/image")
public class ImageService {

	private static final String IMAGE_BUSINESS = "ImageBusiness";
	private static final Integer TWO_HUNDRED = 200;

	private ImageBusinessInterface imageBusinessInterface;

	/* Constructor */

	public ImageService() {

		this.imageBusinessInterface = (ImageBusinessInterface) ApplicationContextConfig
				.getInstance().getAbstractApplicationContext()
				.getBean(IMAGE_BUSINESS);

	}

	@POST
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response insert(final ImageModel imageModel) {

		this.imageBusinessInterface.insert(imageModel);

		return Response.status(TWO_HUNDRED).entity(imageModel).build();
	}

	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(final ImageModel imageModel) {

		ImageModel ImageModelResult = this.imageBusinessInterface.update(imageModel);

		return Response.status(TWO_HUNDRED).entity(ImageModelResult).build();
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/delete")
	public void delete(final ImageModel imageModel) {

		this.imageBusinessInterface.delete(imageModel);

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/select/all")
	public List<ImageModel> selectAll() {

		return this.imageBusinessInterface.selectAll();

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/select/{id}")
	public ImageModel selectById(@PathParam("id") final Long id) {

		return this.imageBusinessInterface.selectById(id);

	}

}
