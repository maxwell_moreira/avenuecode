package br.com.avenuecode.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/* Class responsible for obtaining the validation messages used by the system. */

public class PropertieConfig {

	private static final String CLASS_PATH = "";

	public static final String EMPTY = "";

	private static PropertieConfig configProperties;

	private Properties properties;
	private InputStream inputStream;

	Logger logger = Logger.getLogger(CLASS_PATH);

	public PropertieConfig() {

		try {
			inputStream = new FileInputStream("src/main/resources/config.properties");
			properties = new Properties();
		} catch (FileNotFoundException e) {
			logger.error(e);
		}

	}

	public static synchronized PropertieConfig getInstance() {

		if (configProperties == null)
			configProperties = new PropertieConfig();

		return configProperties;
	}

	public String getMessage(final String key) {

		String message = EMPTY;

		try {

			properties.load(inputStream);
			message = properties.getProperty(key);

		} catch (IOException e) {
			logger.error(e);
		}

		return message;

	}
}