package br.com.avenuecode.config;

import javax.persistence.EntityManager;

/* Class responsible for representing the entity manager. */

public class EntityManagerConfig {
	
	private static EntityManagerConfig entityManagerConfig;
	private EntityManager entityManager;
	
	/* Constructor */
	
	public EntityManagerConfig() {
		
	}
	
	/* Method responsible for obtaining the current instance of the entity manager object. */
	
	public static synchronized EntityManagerConfig getInstance() {
		
		if (entityManagerConfig == null) {
		
			entityManagerConfig = new EntityManagerConfig();
			
		}
		
		return entityManagerConfig;
		
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

}