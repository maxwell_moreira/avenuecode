package br.com.avenuecode.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

/* The central interface within a Spring application for providing configuration information to the application. */

public class ApplicationContextConfig {
	
	private static ApplicationContextConfig factory;
	
	private AbstractApplicationContext abstractApplicationContext;
	
	public ApplicationContextConfig() {
		
		this.abstractApplicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
	
	}
	
	public static synchronized ApplicationContextConfig getInstance() {

		if (factory == null)
			factory = new ApplicationContextConfig();

		return factory;
	}

	public AbstractApplicationContext getAbstractApplicationContext() {
		
		return this.abstractApplicationContext;
	
	}
}
